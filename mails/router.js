var express = require("express");
var router = express.Router();
var Joi = require("joi");
var sendgrid = require("./sendgrid");
var mailgun = require("./mailgun");

const schema = Joi.object({
	to: Joi.array().required(),
	subject: Joi.string().required(),
	cc: Joi.array().optional(),
	bcc: Joi.array().optional(),
	body: Joi.string().required()
}).unknown(false);

router.post("/", function(req, res) {
	const { error } = Joi.validate(req.body, schema);
	if (error !== null) {
		var errMessage = error.details[0].message;
		return res.status(400).json({
			error: {
				message: errMessage,
				type: "Validation failed"
			}
		});
	}

	function handleErrorFromMailgun(err, response, body) {
		if (err || response.statusCode != 200) {
			res.status(500).json({
				error: {
					message: "Couldn't send the mail. Please try again later.",
					type: "Internal server error"
				}
			});
			return;
		}

		res.status(200).send({ message: "Mail sent!" });
		return;
	}

	function handleErrorFromSendgrid(err, response, body) {
		if (!err && response.statusCode == 202) {
			res.status(200).send({ message: "Mail sent!" });
			return;
		}

		if (!err && response.statusCode == 400) {
			res.status(response.statusCode).json({
				error: {
					message: body.errors[0].message,
					type: "Validation failed"
				}
			});
			return;
		}

		mailgun.send(req.body, handleErrorFromMailgun);
	}

	sendgrid.send(req.body, handleErrorFromSendgrid);
});

module.exports = router;
