var request = require("request");

const URL = "https://api.sendgrid.com/v3/mail/send";

function addOptionalKey(options, body, key) {
	if (body[key]) {
		options.json.personalizations[0][key] = mapEmails(body[key]);
	}
}

function mapEmails(emails) {
	return emails.map(t => {
		var obj = {};
		obj["email"] = t;
		return obj;
	});
}

function send(body, cb) {
	if (!process.env.SENDGRID_API_KEY) {
		cb(new Error("SENDGRID_API_KEY is empty."));
		return;
	}

	var options = {
		url: URL,
		auth: {
			bearer: process.env.SENDGRID_API_KEY || ""
		},
		json: {
			from: { email: process.env.FROM_EMAIL || "pawan@dgraph.io" },
			personalizations: [
				{
					to: mapEmails(body.to)
				}
			],
			subject: body.subject,
			content: [{ value: body.body, type: "text/plain" }]
		}
	};

	addOptionalKey(options, body, "cc");
	addOptionalKey(options, body, "bcc");

	request.post(options, cb);
}

module.exports = {
	send: send,
	URL: URL
};
