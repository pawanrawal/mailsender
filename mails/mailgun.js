var request = require("request");

const URL =
	process.env.MAILGUN_URL ||
	"https://api.mailgun.net/v3/mail.pawanrawal.com/messages";

function addOptionalKey(options, body, key) {
	if (body[key]) {
		options.form[key] = body[key].join(",");
	}
}

function send(body, next) {
	var options = {
		url: URL,
		auth: {
			user: process.env.MAILGUN_USER || "",
			pass: process.env.MAILGUN_PASS || ""
		},
		form: {
			from: process.env.FROM_EMAIL || "pawan@dgraph.io",
			to: body.to.join(","),
			subject: body.subject,
			text: body.body
		}
	};

	addOptionalKey(options, body, "cc");
	addOptionalKey(options, body, "bcc");

	request.post(options, next);
}

exports.send = send;
