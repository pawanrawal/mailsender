# Mailsender

Mailsender is a service which allows you to send mails. It uses sendgrid and mailgun in the backend.
Sendgrid is tried first and if it fails we try to use mailgun.

## Getting Started

To set it up locally

1. Clone the repo

```
git clone https://gitlab.com/pawanrawal/mailsender
```

2. Run `npm install`

```
cd mailsender && npm install
```

3. Start the app

```
export SENDGRID_API_KEY=<key>
export MAILGUN_USER=<user>
export MAILGUN_PASS=<pass>
npm run start
```

The user interface should be available on [http://localhost:3000](http://localhost:3000).

Note - Only one set(sendgrid/mailgun) of credentials are required. Though having both makes our service
really robust and allows for failover.

## Example

A example working app is deployed at [https://mailsender-siteminder.herokuapp.com](https://mailsender-siteminder.herokuapp.com/).
The app is deployed using [Heroku](https://devcenter.heroku.com/articles/deploying-nodejs).
All the credentials are passed as environment variables.

## TODO

* Store sent mails and have a page where you can view them.
* Add debug mode logging.
* Validate input type of environment variables.

## Tests

```
npm run test
```