const request = require("supertest");
const express = require("express");
const assert = require("chai").assert;
const nock = require("nock");
const sinon = require("sinon");

var sendgrid = require("../mails/sendgrid");
var mailgun = require("../mails/mailgun");
const app = require("../app.js");

describe("GET /blah", function() {
	it("responds with 404 on wrong url", function(done) {
		request(app)
			.get("/blah")
			.expect(404)
			.end(function(err, res) {
				if (err) return done(err);
				done();
			});
	});
});

describe("POST /mails", function() {
	it("responds with field required validation error", function(done) {
		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.expect(400)
			.end(function(err, res) {
				if (err) return done(err);
				assert.deepEqual(res.body, {
					error: {
						message: '"to" is required',
						type: "Validation failed"
					}
				});
				done();
			});
	});

	it("responds with is not allowed validation error", function(done) {
		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.send({
				name: "mocha",
				to: ["alice"],
				subject: "sub",
				body: "body"
			})
			.expect(400)
			.end(function(err, res) {
				if (err) return done(err);
				assert.deepEqual(res.body, {
					error: {
						message: '"name" is not allowed',
						type: "Validation failed"
					}
				});
				done();
			});
	});

	it("responds with wrong type validation error", function(done) {
		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.send({ body: ["mocha"], to: ["alice"], subject: "sub" })
			.expect(400)
			.end(function(err, res) {
				if (err) return done(err);
				assert.deepEqual(res.body, {
					error: {
						message: '"body" must be a string',
						type: "Validation failed"
					}
				});
				done();
			});
	});

	it("responds with subject required validation error", function(done) {
		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.send({ body: "mocha", to: ["alice"], subject: "" })
			.expect(400)
			.end(function(err, res) {
				if (err) return done(err);
				assert.deepEqual(res.body, {
					error: {
						message: '"subject" is not allowed to be empty',
						type: "Validation failed"
					}
				});
				done();
			});
	});

	it("responds with 200 when mail is sent by sendgrid", function(done) {
		var scope = nock(sendgrid.URL)
			.filteringRequestBody(function(body) {
				return "*";
			})
			.post("", "*")
			.reply(202, {});

		var sg_spy = sinon.spy(sendgrid, "send");
		var mg_spy = sinon.spy(mailgun, "send");

		process.env.SENDGRID_API_KEY = "dummy";

		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.send({ body: "body", to: ["abc@gmail.com"], subject: "sub" })
			.expect(200)
			.end(function(err, res) {
				if (err) return done(err);
				assert.equal(res.text, '{"message":"Mail sent!"}');
				scope.done();
				sinon.assert.calledOnce(sg_spy);
				sinon.assert.notCalled(mg_spy);

				sg_spy.restore();
				mg_spy.restore();
				done();
			});
	});

	it("responds with 200 when mail is sent by mailgun", function(done) {
		var scope = nock(/api\.+/); // match all hosts
		nock.cleanAll();
		// Sendgrid should reply with 401 whereas mailgun with 200.
		scope.intercept("/v3/mail/send", "post").reply(401, {
			errors: [{ message: "Not authorized" }]
		});
		scope
			.intercept("/v3/mail.pawanrawal.com/messages", "post")
			.reply(200, {});

		var sg_spy = sinon.spy(sendgrid, "send");
		var mg_spy = sinon.spy(mailgun, "send");

		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.send({ body: "body", to: ["abc@gmail.com"], subject: "sub" })
			.expect(200)
			.end(function(err, res) {
				if (err) return done(err);
				assert.equal(res.text, '{"message":"Mail sent!"}');
				scope.done();
				sinon.assert.calledOnce(sg_spy);
				sinon.assert.calledOnce(mg_spy);

				sg_spy.restore();
				mg_spy.restore();
				done();
			});
	});

	it("responds with 500 when both services are down", function(done) {
		var scope = nock(/api\.+/); // match all hosts
		nock.cleanAll();
		scope.intercept("/v3/mail/send", "post").reply(500, {});
		scope
			.intercept("/v3/mail.pawanrawal.com/messages", "post")
			.reply(400, {});

		var sg_spy = sinon.spy(sendgrid, "send");
		var mg_spy = sinon.spy(mailgun, "send");

		request(app)
			.post("/mails")
			.set("Accept", "application/json")
			.send({ body: "body", to: ["abc@gmail.com"], subject: "sub" })
			.expect(500)
			.end(function(err, res) {
				if (err) return done(err);
				assert.deepEqual(res.body, {
					error: {
						message:
							"Couldn't send the mail. Please try again later.",
						type: "Internal server error"
					}
				});
				scope.done();
				sinon.assert.calledOnce(sg_spy);
				sinon.assert.calledOnce(mg_spy);

				sg_spy.restore();
				mg_spy.restore();
				done();
			});
	});
});
