$(document).ready(function() {
	$(".emails").select2({
		tags: true
	});

	$("#mail").submit(function(e) {
		event.preventDefault();

		form = $("#mail")[0];

		if (form.checkValidity() === false) {
			event.stopPropagation();
			form.classList.add("was-validated");
			return;
		}

		form.classList.add("was-validated");

		var url = "/mails";
		var data = $("#mail").serializeJSON();

		var submitButton = $("#mail :submit");
		submitButton.prop("disabled", true).html("Sending...");

		$.ajax({
			type: "POST",
			url: url,
			data: JSON.stringify(data),
			contentType: "application/json",
			dataType: "json",
			success: function(data) {
				$(".alert")
					.removeClass("alert-danger")
					.addClass("alert-success")
					.text(data.message)
					.show();
				$(".emails")
					.val("")
					.trigger("change");

				form.reset();
				form.classList.remove("was-validated");
				setTimeout(function() {
					$(".alert").hide();
				}, 5000);
			},
			error: function(data) {
				var errorText;
				if (data.responseJSON) {
					errorText = data.responseJSON.error.message;
				} else if (data.status === 0) {
					errorText = "No connection to server. Please try again.";
				} else {
					errorText = data.statusText;
				}

				$(".alert")
					.removeClass("alert-success")
					.addClass("alert-danger")
					.text(errorText)
					.show();
			},
			complete: function() {
				window.scrollTo(0, 0);
				submitButton.prop("disabled", false).html("Send");
			}
		});
	});
});
